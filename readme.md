# Athena

Athena is a simple python script for arp cache poisoning

## Installation

In order to run this script you must install one third party application depndent on your platform.

### Debian
For Debian you need tcpdump:
```bash
sudo apt-get install tcpdump
```

### Mac OS X
For Mac OS X you need libcap:
```bash
brew update
brew install libpcap
```
### Windows
For Windows you need [npcap](https://nmap.org/npcap/#download)

After you have installed your third party application you must install scapy:
```bash
pip install scapy
```

## Usage

To run the script just type
```bash
python athena.py target_ip [-v]
```
The -h or --help option prints an help message.
With -v or --verbose Athena prints a summary message of what is happening.

## Features

- Athena spoof the connection between the target ip addres (in your LAN network) and your local gateway.
- It works even with wifi clients.
- No need for mac addresses, Athena will find them for you.
- Once Athena detects CTRL+C she restores the connection between the target and the gateway.
- Athena doesn't enable ip forwarding so packets recevied from the target are NOT sent to the gateway, the result will be a DOS.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)